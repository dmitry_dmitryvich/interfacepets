package ru.bokov;

/**
 * Класс цыплёнок
 *
 * @Author Боков Дмитрий
 */
public class Chicken implements Pets{
    @Override
    public String typeOfAnimal(){
            return "Цыплёнок ";
        }

    public String nameOfAnimal(){
            return " Василий ";
        }

    public String voiceOfAnimal(){
            return " Пи-Пи";
        }

    public String toString(){
        return (typeOfAnimal() + nameOfAnimal() + "говорит" + voiceOfAnimal());
    }
}
