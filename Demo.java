package ru.bokov;

/**
 * Демо-класс, демонстритующий интерфейс Pets.
 *
 * @Author Боков Дмитрий
 */
public class Demo {
    public static void main(String[] args) {
        Dog dogOfWar = new Dog();
        System.out.println(dogOfWar);
        Cat Sonya = new Cat();
        System.out.println(Sonya);
        Cow byrenka = new Cow();
        System.out.println(byrenka);
        Chicken chick = new Chicken();
        System.out.println(chick);

    }
}
