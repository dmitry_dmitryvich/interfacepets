package ru.bokov;

/**
 * Интерфейс Pets.
 *
 * @Author Боков Дмитрий
 */
public interface Pets {
    /**
     * typeOfAnimal() - метод, задающий тип животного
     * nameOfAnimal() - метод, задающий имя животного
     * voiceOfAnimal() -метод, задающий звук животного
     * toString() - метод вывода
     */
    String typeOfAnimal();
    String nameOfAnimal() ;
    String voiceOfAnimal();
    String toString();
}
