package ru.bokov;

/**
 * Класс Собачка
 *
 * @Author Боков Дмитрий
 */
public class Dog implements Pets {
    @Override
    public String typeOfAnimal(){
        return "Пес";
    }

    public String nameOfAnimal(){
        return " Воин ";
    }

    public String voiceOfAnimal(){
        return " Гав-Гав ";
    }

    public String toString(){
        return (typeOfAnimal() + nameOfAnimal() + "говорит" + voiceOfAnimal());
    }

}
