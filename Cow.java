package ru.bokov;

/**
 * Класс Корова
 *
 * @Author Боков Дмитрий
 */
public class Cow implements Pets{
    @Override
    public String typeOfAnimal(){
        return "Корова ";
    }

    public String nameOfAnimal(){
        return " Буренка ";
    }

    public String voiceOfAnimal(){
        return " Мууу-Мууу";
    }

    public String toString(){
        return (typeOfAnimal() + nameOfAnimal() + "говорит" + voiceOfAnimal());
    }
}
