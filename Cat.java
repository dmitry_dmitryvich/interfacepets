package ru.bokov;

/**
 * Класс Кошка
 *
 * @Author Боков Дмитрий
 */
public class Cat implements Pets {
    @Override
    public String typeOfAnimal(){
            return "Кошка";
        }

    public String nameOfAnimal(){
            return " Соня ";
        }

    public String voiceOfAnimal(){
            return " Муур";
        }

    public String toString(){
        return (typeOfAnimal() + nameOfAnimal() + "говорит" + voiceOfAnimal());
    }
}
